import json
import requests
import urllib.parse
from unidecode import unidecode

# HQ_BACKEND               = 'http://msv-testing.local:8888'
# HQ_BACKEND               = 'http://qa-hq-backend.seven-system.com'
HQ_BACKEND               = 'https://backend.sevensystem.vn'
# HQ_BACKEND               = 'https://staging-backend.sevensystem.vn'
# HQ_BACKEND               = 'http://uat-hq-backend.seven-system.com'
# HQ_BACKEND               = 'http://dev-hq-backend.seven-system.com'
HQ_AUTHENTICATION        = HQ_BACKEND + '/api/v2/hq/authentication'
HQ_GET_SUPPLIERS         = HQ_BACKEND + '/api/v2/hq/suppliers?&q=&page_size=9999&\{\}'
HQ_SUPPLIER              = HQ_BACKEND + '/api/v2/hq/suppliers/basic'
HQ_SP_CONTACT            = HQ_BACKEND + '/api/v2/hq/suppliers/contact/{}'
HQ_SP_PAYMENT            = HQ_BACKEND + '/api/v2/hq/suppliers/payment/term/{}'
HQ_SP_TRADING            = HQ_BACKEND + '/api/v2/hq/suppliers/trading/term/{}'
HQ_CITIES                = HQ_BACKEND + '/api/v2/hq/cities?&q=&\{\}'
HQ_DISTRICTS             = HQ_BACKEND + '/api/v2/hq/districts?&q=&\{\}'
HQ_WARDS                 = HQ_BACKEND + '/api/v2/hq/wards?&q=&\{\}'
HQ_BRANDS                = HQ_BACKEND + '/api/v2/hq/supplier/branches?&q=&\{\}'
HQ_PRODUCT_BRANDS        = HQ_BACKEND + '/api/v2/hq/brands/?&page=1&page_size=9999&\{\}'
HQ_PRODUCT_MANUFACTURERS = HQ_BACKEND + '/api/v2/hq/manufacturers/?&page=1&page_size=9999&\{\}'
HQ_LOGISTICS_GROUP       = HQ_BACKEND + '/api/v2/hq/logistics/?&page=1&page_size=9999&\{\}'
HQ_PRODUCT_BASIC         = HQ_BACKEND + '/api/v2/hq/products/basic'
HQ_PRODUCT_SUPPLIER      = HQ_BACKEND + '/api/v2/hq/product_supplier_cdc'
HQ_PRODUCT_UOM           = HQ_BACKEND + '/api/v2/hq/product_uoms'
HQ_COUNTRIES             = HQ_BACKEND + '/api/v2/hq/countries?&q=&\{\}'
HQ_INPUT_VAT             = HQ_BACKEND + '/api/v2/hq/vats?&q=&vat_type=0&single_page=true&\{\}'
HQ_OUTPUT_VAT            = HQ_BACKEND + '/api/v2/hq/vats?&q=&vat_type=1&single_page=true&\{\}'
HQ_SUB_CATEGORY          = HQ_BACKEND + '/api/v2/hq/categories?&q=&type=2&single_page=true&\{\}'
HQ_CATEGORIES            = HQ_BACKEND + '/api/v2/hq/categories'
HQ_PRODUCT_RETAIL_PRICE  = HQ_BACKEND + '/api/v2/hq/mapping/retail_price_sale_area/{}'
HQ_PRODUCT               = HQ_BACKEND + '/api/v2/hq/products'
HQ_PRODUCT_LIST          = HQ_BACKEND + '/api/v2/hq/products/?single_page=true&sort_property=id&sort_direction=DESC&\{\}'

# EMAIL = 'admin@ssv.com'
# PASSWORD = '123456'
EMAIL = 'hoangviet@msv-tech.vn'
PASSWORD = 'viethuyen2602'
ACCESS_TOKEN = ''


def post_access():
    '''Post access and return access key'''
    access_json = "{{\"email\": \"{0}\", \"password\": \"{1}\"}}".format(
        EMAIL, PASSWORD)
    global ACCESS_TOKEN
    if not ACCESS_TOKEN:
        respone = requests.post(
            HQ_AUTHENTICATION, None, json=json.loads(access_json))

        if respone.raise_for_status():
            respone.raise_for_status()
        else:
            respone = respone.json()

        access_token = respone['access_token']
        ACCESS_TOKEN = respone['access_token']
    else:
        access_token = ACCESS_TOKEN

    # access_token = respone['access_token']
    return access_token


def post_supplier(supplier_json):
    '''Post Supplier basic info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        HQ_SUPPLIER,
        headers=json.loads(header),
        json=supplier_json)
    return respone


def put_supplier_basic(product_id, supplier_json):
    '''Put Supplier basic info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_SUPPLIER + '/{}'.format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=supplier_json)
    return respone


def get_supplier_basic(product_id):
    '''Get Supplier basic info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_SUPPLIER + '/{}'.format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header))
    return respone.json()


def get_suppliers():
    '''Get list suppliers'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_GET_SUPPLIERS,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def get_brands():
    '''Get brands'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(HQ_BRANDS, headers=json.loads(header))
    return respone.text


def get_cities():
    '''Get cities'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(HQ_CITIES, headers=json.loads(header))
    return respone.text


def get_districts():
    '''Get districts'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(HQ_DISTRICTS, headers=json.loads(header))
    return respone.text


def get_wards():
    '''Get brands'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(HQ_WARDS, headers=json.loads(header))
    return respone.text


def put_supplier_contact(supplier_id, sp_contact_json):
    '''Put supplier contact info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_SP_CONTACT.format(supplier_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=sp_contact_json)
    return respone


def put_supplier_payment(supplier_id, payment_json):
    '''Put supplier payment info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_SP_PAYMENT.format(supplier_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=payment_json)
    return respone


def put_supplier_trading(supplier_id, trading_json):
    '''Put supplier trading info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_SP_TRADING.format(supplier_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=trading_json)
    return respone


def get_product_brands():
    '''Get all product brands'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_PRODUCT_BRANDS,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def get_logistics_group():
    '''Get all logistics group'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_LOGISTICS_GROUP,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def get_product_manufacturers():
    '''Get all product manufacturers'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_PRODUCT_MANUFACTURERS,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def post_product_brands(brands_json):
    '''Put product brands info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        HQ_PRODUCT_BRANDS,
        headers=json.loads(header),
        json=brands_json)
    return respone


def post_product_manufacturers(manufacturers_json):
    '''Put product manufacturers info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        HQ_PRODUCT_MANUFACTURERS,
        headers=json.loads(header),
        json=manufacturers_json)
    return respone


def post_product_basic(product_json):
    '''Post product basic info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        HQ_PRODUCT_BASIC,
        headers=json.loads(header),
        json=product_json)
    return respone


def put_product_uom(product_id, product_uom_json):
    '''Put product uom'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT_UOM + "/{}".format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=product_uom_json)
    return respone


def put_product_supplier(product_id, product_supplier_json):
    '''Put product supplier'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    # print(json.dumps(product_supplier_json))
    url = HQ_PRODUCT_SUPPLIER + "/{}".format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=product_supplier_json)
    return respone


def put_product_retail_price(product_id, product_retail_price_json):
    '''Put product supplier sale area'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT_RETAIL_PRICE.format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=product_retail_price_json)
    return respone


def get_product_retail_price(product_id):
    '''Get product supplier retail price'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT_RETAIL_PRICE.format(product_id)
    respone = requests.get(
        url,
        headers=json.loads(header))
    return respone.json()


def get_product_uom(product_id):
    '''Get product uom'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT_UOM + "/{}".format(product_id) + "?&q=&\{\}"
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()['product_uoms']
    return content


def get_sub_category():
    '''Get sub category'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_SUB_CATEGORY,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def get_countries():
    '''Get countries list'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_COUNTRIES,
        headers=json.loads(header))
    content = respone.json()
    return content


def get_input_vat():
    '''Get input vat'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_INPUT_VAT,
        headers=json.loads(header))
    content = respone.json()
    return content


def get_output_vat():
    '''Get output vat'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_OUTPUT_VAT,
        headers=json.loads(header))
    content = respone.json()
    return content


def post_categories(info_json):
    '''Post groups, categories, sub categories info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.post(
        HQ_CATEGORIES,
        headers=json.loads(header),
        json=info_json)
    return respone


def get_categories(category_type):
    '''Get output vat'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    param = '/?&q=&type={}'.format(category_type)
    url = HQ_CATEGORIES + param + '&single_page=true&\{\}'
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()['content']
    return content


def get_product_id(product_name):
    '''Get product id by product name'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    param = '/?q=' + urllib.parse.quote_plus(product_name) + '&sort_property=id&sort_direction=DESC&\{\}'
    url = HQ_PRODUCT + param
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()['content']
    p_id = None
    short_name = None
    if not content:
        content = None
    else:
        content = content[0]
        p_id = content['id']
        short_name = content['short_name']
    return p_id, short_name


def put_product_basic(product_id, info_json):
    '''Update product basic info'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT_BASIC + '/{}'.format(product_id)
    respone = requests.put(
        url,
        headers=json.loads(header),
        json=info_json)
    return respone


def get_products_list():
    '''Get all products'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    respone = requests.get(
        HQ_PRODUCT_LIST,
        headers=json.loads(header))
    content = respone.json()['content']
    if not content:
        content = None
    return content


def get_products_basic(product_id):
    '''Get products basic'''
    access_token = post_access()
    header = "{{\"content-type\": \"application/json\",\"authorization\":\
                    \"bearer {0}\",\"cache-control\": \"no-cache\"}}".format(
        access_token)
    url = HQ_PRODUCT + "/{}".format(product_id) + "?false"
    respone = requests.get(
        url,
        headers=json.loads(header))
    content = respone.json()
    if not content:
        content = None
    return content
