import logging
import pandas as pd
from unidecode import unidecode
import api_requests
from logging.config import fileConfig

# LOG_FILE = 'logger_update_products.log'
fileConfig('logging_config.ini')
logger = logging.getLogger()
PRODUCT_BASIC = {
    "barcode_type": '',
    "brand_id": '',
    "brand_type": '',
    "country_code": '',
    "detail_name_en": '',
    "detail_name": '',
    "expiration_shelf_life_unit": '',
    "final_approve": '',
    "input_vat_id": '',
    "manufacturer_id": '',
    "md_approve": '',
    "minimum_display_quantity": '',
    "output_vat_id": '',
    "preservation_temperature": '',
    "price_tag_issue_flag": '',
    "product_specification": '',
    "retail_business_type": '',
    "sales_limitation": '',
    "shipping_limitation": '',
    "short_name_en": '',
    "short_name": '',
    "total_shelf_life": '',
    "writeoffable": '',
    "sub_category_id": ''
}
PRODUCT_RETAIL_FIRST = {
    "product_uom_id": '',
    "product_vendor_mapping_id": '',
    "retail_selling_price_with_tax": '',
    "new_retail_selling_price_with_tax": '',
    "retail_selling_price_effective_date": '',
    "new_retail_selling_price_effective_date": ''
}
PRODUCT_RETAIL_SECOND = {
    "product_uom_id": '',
    "product_vendor_mapping_id": '',
    "retail_selling_price_with_tax": '',
    "new_retail_selling_price_with_tax": '',
    "retail_selling_price_effective_date": '',
    "new_retail_selling_price_effective_date": ''
}


def get_id(product_name):
    prod_id, prod_short_name = api_requests.get_product_id(product_name)
    return prod_id


def get_products_id(file_path):
    '''Get products id and return to products dataframe'''
    products_list = api_requests.get_products_list()
    products_list = pd.DataFrame(products_list)
    products_list['short_name_check'] = products_list.short_name
    products_list.short_name = products_list.short_name.astype(str)
    products_list.short_name = products_list.short_name.apply(
        lambda x: unidecode(x))

    # Remove white space in short name
    products_list.short_name = products_list.short_name.astype(str).str.strip()
    products_list = products_list[[
        'short_name', 'id', 'status', 'product_code', 'short_name_check']].sort_index()

    # Read list products removed
    prod_removed = pd.read_excel(file_path)
    prod_removed.short_name = prod_removed.short_name.astype(str).str.strip()
    prod_removed.short_name = prod_removed.short_name.apply(
        lambda x: unidecode(x)
    )
    prod_removed_list = prod_removed.short_name.tolist()
    prod_removed.short_name.to_csv("temp.csv")

    # Get products id and return to products removed
    products_list = products_list[products_list.short_name.isin(
        prod_removed_list)]

    # products_list['id'] = products_list['short_name'].apply(
    #     lambda x: get_id(x)
    # )
    # for index, row in products_list.iterrows():
    #     row['id'] = get_id(row['short_name'])

    excel = pd.ExcelWriter('Active_products_with_id.xlsx')
    products_list.to_excel(excel, 'Active_products_with_id')
    excel.save()

    return products_list


def active_products(products_list: pd.DataFrame):
    '''Active products'''
    global PRODUCT_BASIC
    global PRODUCT_RETAIL_FIRST
    global PRODUCT_RETAIL_SECOND

    prod_price_first = dict(PRODUCT_RETAIL_FIRST)
    prod_price_second = dict(PRODUCT_RETAIL_SECOND)

    for index, row in products_list.iterrows():
        basic_info = api_requests.get_products_basic(row.id)
        PRODUCT_BASIC['barcode_type'] = basic_info['barcode_type']
        PRODUCT_BASIC['brand_id'] = basic_info['brand_id']
        PRODUCT_BASIC['brand_type'] = basic_info['brand_type']
        PRODUCT_BASIC['country_code'] = basic_info['country_code']
        PRODUCT_BASIC['detail_name_en'] = basic_info['detail_name_en']
        PRODUCT_BASIC['detail_name'] = basic_info['detail_name']
        PRODUCT_BASIC['expiration_shelf_life_unit'] = basic_info['expiration_shelf_life_unit']
        PRODUCT_BASIC['final_approve'] = "true"
        PRODUCT_BASIC['input_vat_id'] = basic_info['input_vat_id']
        PRODUCT_BASIC['manufacturer_id'] = basic_info['manufacturer_id']
        PRODUCT_BASIC['md_approve'] = "true"
        PRODUCT_BASIC['minimum_display_quantity'] = basic_info['minimum_display_quantity']
        PRODUCT_BASIC['output_vat_id'] = basic_info['output_vat_id']
        PRODUCT_BASIC['preservation_temperature'] = basic_info['preservation_temperature']
        PRODUCT_BASIC['price_tag_issue_flag'] = basic_info['price_tag_issue_flag']
        PRODUCT_BASIC['product_specification'] = basic_info['product_specification']
        PRODUCT_BASIC['retail_business_type'] = basic_info['retail_business_type']
        PRODUCT_BASIC['sales_limitation'] = basic_info['sales_limitation']
        PRODUCT_BASIC['shipping_limitation'] = basic_info['shipping_limitation']
        PRODUCT_BASIC['short_name_en'] = basic_info['short_name_en']
        PRODUCT_BASIC['short_name'] = basic_info['short_name']
        PRODUCT_BASIC['total_shelf_life'] = basic_info['total_shelf_life']
        PRODUCT_BASIC['writeoffable'] = basic_info['writeoffable']
        PRODUCT_BASIC['sub_category_id'] = basic_info['sub_category_id']

        logger.debug(PRODUCT_BASIC)
        logger.debug(api_requests.put_product_basic(row.id, PRODUCT_BASIC))

        retail_price_detail = api_requests.get_product_retail_price(row.id)
        retail_price_detail = retail_price_detail['retail_price_sale_area_mapping_details']
        first_price = retail_price_detail[0]

        prod_price_first["product_uom_id"] = first_price["product_uom_id"]
        prod_price_first["product_vendor_mapping_id"] = first_price["product_vendor_mapping_id"]
        prod_price_first["retail_selling_price_with_tax"] = first_price["retail_selling_price_with_tax"]
        prod_price_first["new_retail_selling_price_with_tax"] = first_price["new_retail_selling_price_with_tax"]
        prod_price_first["retail_selling_price_effective_date"] = first_price["retail_selling_price_effective_date"]
        prod_price_first["new_retail_selling_price_effective_date"] = first_price["new_retail_selling_price_effective_date"]

        if len(retail_price_detail) > 1:
            second_price = retail_price_detail[1]
            prod_price_second["product_uom_id"] = second_price["product_uom_id"]
            prod_price_second["product_vendor_mapping_id"] = second_price["product_vendor_mapping_id"]
            prod_price_second["retail_selling_price_with_tax"] = second_price["retail_selling_price_with_tax"]
            prod_price_second["new_retail_selling_price_with_tax"] = second_price["new_retail_selling_price_with_tax"]
            prod_price_second["retail_selling_price_effective_date"] = second_price["retail_selling_price_effective_date"]
            prod_price_second["new_retail_selling_price_effective_date"] = second_price["new_retail_selling_price_effective_date"]

            retail_price_info = {
                "apply_all_sale_area": True,
                "retail_price_sale_area_mapping_form_details": [
                    prod_price_first,
                    prod_price_second
                ]
            }
        else:
            retail_price_info = {
                "apply_all_sale_area": True,
                "retail_price_sale_area_mapping_form_details": [prod_price_first]}

        logger.debug(retail_price_info)
        logger.debug(api_requests.put_product_retail_price(
            row.id,
            retail_price_info).text)

    excel = pd.ExcelWriter('Active_products_with_id_.xlsx')
    products_list.to_excel(excel, 'temp')
    excel.save()


if __name__ == "__main__":
    products_list = get_products_id('./active_products.xlsx')
    # products_list = pd.DataFrame(pd.read_excel('./active_products.xlsx'))
    # active_products(products_list)
    # print(get_id('Gum Ice Breakers Pepermint'))
