import product_master_new
import logging
from logging.config import fileConfig
import pandas as pd

fileConfig('logging_config.ini')
logger = logging.getLogger()


if __name__ == "__main__":
    # Read product master excel and create import info excel
    a, b, c, d, e = product_master_new.read_product_master(
        './Product_Master_JUN08.xlsx',
        'Sheet1',
        header=0)
    # a = pd.DataFrame(a.iloc[-5:])
    # print(a)
    excel = pd.ExcelWriter('product_output.xlsx')
    a.to_excel(excel, 'product_basic')
    b.to_excel(excel, 'product_uom')
    c.to_excel(excel, 'product_uom_alternative')
    d.to_excel(excel, 'product_supplier')
    excel.save()

    ex_result = pd.ExcelWriter('product_basic_result_temp_.xlsx')
    # rs_basic_info, failures = product_master_new.post_product_basic(a)
    # rs_basic_info.to_excel(ex_result, 'basic')
    # ex_result.save()
    # logger.debug(failures)

    # In case failed at update UOM, Open this Block
    rs_basic_info = pd.read_excel('product_basic_result_temp_.xlsx', 'basic')
    rs_basic_info = rs_basic_info[['short_name', 'id']]
    product_basic_uom_merged = pd.merge(rs_basic_info, b, on='short_name')
    product_basic_alter_uom_merged = pd.merge(
        rs_basic_info,
        c,
        on='short_name')

    rs_uom_info, failures = product_master_new.put_product_uom(
        product_basic_uom_merged,
        product_basic_alter_uom_merged)
    rs_uom_info.to_excel(ex_result, 'uom')
    ex_result.save()
    logger.debug(failures)

    # In case failed at update supplier, Open this block
    rs_uom_info = pd.DataFrame(
        pd.read_excel('product_basic_result_temp_.xlsx', 'uom'))
    # print(rs_uom_info)
    product_basic_supplier_merged = pd.merge(
        rs_uom_info,
        d,
        left_on='product_name',
        right_on='short_name')
    rs_product_supplier, failures = product_master_new.put_product_supplier(
        product_basic_supplier_merged)
    rs_product_supplier.to_excel(ex_result, 'supplier')
    ex_result.save()
    logger.debug(failures)

    # In case failed update retail price, Open this block
    # rs_product_supplier = pd.read_excel('product_basic_result.xlsx', 'uom')
    rs_product_supplier = rs_product_supplier[
        ['product_name', 'product_id']]
    product_retail_price_merged = pd.merge(
        rs_product_supplier,
        e,
        left_on='product_name',
        right_on='short_name')
    # print(product_retail_price_merged)
    rs_product_retail_price, failures = product_master_new.put_product_retail_price(
            product_retail_price_merged)
    rs_product_retail_price.to_excel(ex_result, 'retail')
    ex_result.save()
    logger.debug(failures)
