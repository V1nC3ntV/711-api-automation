import json
import datetime
import time
import re
import ast
import pandas as pd
import numpy as np
import api_requests
import convert_vn

PRODUCT_CYCLE_TYPE = {"Non Daily": 0, "Daily": 1, "Other": 2}
GROUPS             = ['group_no', 'name', 'description', 'type', 'product_cycle_type']
CATEGORIES         = ['group_no', 'name', 'description', 'type', 'parent_id']
SUB_CATEGORIES     = ['group_no', 'name', 'description', 'type', 'parent_id']


def read_categories(excel_file):
    categories_master_df = pd.DataFrame(pd.read_excel(excel_file))
    categories_master_df = categories_master_df.replace({'Product Cycle Type': PRODUCT_CYCLE_TYPE}, regex=True)

    # Replace NaN with empty
    categories_master_df.fillna(value="", inplace=True)

    groups_df         = pd.DataFrame(columns = GROUPS)
    categories_df     = pd.DataFrame(columns = CATEGORIES)
    sub_categories_df = pd.DataFrame(columns = SUB_CATEGORIES)

    # Mapping Group info
    groups_df['name']                = categories_master_df['Product Group (PMA)'].str.strip()
    groups_df['group_no']            = categories_master_df['Product Group ID: 2-Digits']
    groups_df['description']         = ""
    groups_df['type']                = 0
    groups_df['product_cycle_type']  = categories_master_df['Product Cycle Type']
    groups_df                        = groups_df.drop_duplicates('name')

    # Mapping Categories info
    categories_df['name']            = categories_master_df['Product Category'].str.strip()
    categories_df['group_no']        = categories_master_df['Product Category ID: Product Group ID + 2-Digits'].astype(str)
    categories_df['description']     = ""
    categories_df['type']            = 1
    categories_df['parent_id']       = categories_master_df['Product Group (PMA)'].str.strip()
    categories_df                    = categories_df.drop_duplicates('name')

    # Mapping Sub Categories info
    sub_categories_df['name']        = categories_master_df['Product Sub-Category'].str.strip()
    sub_categories_df['group_no']    = categories_master_df['Product Sub-Category ID: Product Cat ID + 2-Digits'].astype(str)
    sub_categories_df['description'] = ""
    sub_categories_df['type']        = 2
    sub_categories_df['parent_id']   = categories_master_df['Product Category'].str.strip()
    sub_categories_df                = sub_categories_df.drop_duplicates('name')

    # Mapping id
    categories_df['group_no']    = categories_df['group_no'].str[-2: ].astype(int)
    sub_categories_df['group_no'] = sub_categories_df['group_no'].str[-2:].astype(int)

    return groups_df, categories_df, sub_categories_df


def post_groups(groups_df: pd.DataFrame):
    response_success = list()
    response_failed  = list()
    groups_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in groups_df.iterrows():
        groups_info = json.loads(row.to_json())
        groups_info['name'] = str.strip(groups_info['name'])
        response = api_requests.post_categories(groups_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(groups_info)
    response_success_df = pd.DataFrame(pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def post_categories(categories_df: pd.DataFrame):
    response_success = list()
    response_failed  = list()
    categories_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()

    group_id = api_requests.get_categories(0)
    group_id = pd.DataFrame(group_id)
    group_id = group_id[['group_name', 'group_id']].set_index('group_name')
    group_id = group_id.to_dict()['group_id']
    categories_df = categories_df.replace({'parent_id': group_id})
    categories_df = categories_df.drop_duplicates('name')

    for index, row in categories_df.iterrows():
        categories_info = json.loads(row.to_json())
        categories_info['name'] = str.strip(categories_info['name'])
        response = api_requests.post_categories(categories_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(categories_info)
    response_success_df = pd.DataFrame(pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def post_sub_categories(sub_categories_df: pd.DataFrame):
    response_success = list()
    response_failed  = list()
    sub_categories_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()

    category_id = api_requests.get_categories(1)
    category_id = pd.DataFrame(category_id)
    category_id = category_id[['category_name', 'category_id']].set_index('category_name')
    category_id = category_id.to_dict()['category_id']
    sub_categories_df = sub_categories_df.replace({'parent_id': category_id})
    sub_categories_df = sub_categories_df.drop_duplicates('name')

    for index, row in sub_categories_df.iterrows():
        sub_categories_info = json.loads(row.to_json())
        response = api_requests.post_categories(sub_categories_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(sub_categories_info)
    response_success_df = pd.DataFrame(pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


a, b ,c = read_categories('categories_master.xlsx')
# print(a.count())
# print(b)
# print(c)

rs_group, failures = post_groups(a)
print(failures)

# excel = pd.ExcelWriter('categories_output.xlsx')
# b.to_excel(excel, 'categories')
# excel.save()

rs_category, failures = post_categories(b)
print(failures)

rs_sub_categories, failures = post_sub_categories(c)
print(failures)
