import json
import logging
import os
import pandas as pd
import api_requests
import product_master_new
from logging.config import fileConfig

# LOG_FILE = 'logger_update_products.log'
fileConfig('logging_config.ini')
logger = logging.getLogger()
PRODUCT_BASIC = {
    "barcode_type": '',
    "brand_id": '',
    "brand_type": '',
    "country_code": '',
    "detail_name_en": '',
    "detail_name": '',
    "expiration_shelf_life_unit": '',
    "final_approve": '',
    "input_vat_id": '',
    "manufacturer_id": '',
    "md_approve": '',
    "minimum_display_quantity": '',
    "output_vat_id": '',
    "preservation_temperature": '',
    "price_tag_issue_flag": '',
    "product_specification": '',
    "retail_business_type": '',
    "sales_limitation": '',
    "shipping_limitation": '',
    "short_name_en": '',
    "short_name": '',
    "total_shelf_life": '',
    "writeoffable": '',
    "sub_category_id": '',
}


def update_product_basic(product_pd: pd.DataFrame):
    '''Update product basic'''
    response_success = list()
    response_failed = list()
    response_success_df = pd.DataFrame()

    for index, row in product_pd.iterrows():
        product_info = json.loads(row.to_json())
        product_id, product_short_name = api_requests.get_product_id(
            product_info['short_name'])
        logger.debug(product_id)
        logger.debug(product_info['short_name'])
        logger.debug(product_short_name)

        if product_id is not None:
            logger.debug(product_id)
            response = api_requests.put_product_basic(product_id, product_info)
            if response.status_code in [200, 201]:
                response_success.append(response.json())
                # logger.debug(response_success)
            else:
                response_failed.append(response.text)
                response_failed.append(product_info)
        else:
            logger.debug(product_info['short_name'])
    # logger.debug(product_id + '-' + response)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def get_products_id(file_path):
    '''Get products id and return to products dataframe'''
    products_list = api_requests.get_products_list()
    products_list = pd.DataFrame(products_list)

    # Remove white space in short name
    products_list.short_name = products_list.short_name.str.strip()
    products_list = products_list[['short_name', 'id']]

    # Read list products removed
    prod_removed = pd.read_excel(file_path)
    prod_removed.short_name = prod_removed.short_name.str.strip()
    prod_removed_list = prod_removed.short_name.tolist()

    # Get products id and return to products removed
    products_list = products_list[products_list.short_name.isin(
        prod_removed_list)]

    excel = pd.ExcelWriter('Removed_products_with_id.xlsx')
    products_list.to_excel(excel, 'Removed_products_with_id')
    excel.save()

    return products_list


def change_uom(uoms: list, product_id):
    product_uom = list()
    xstr = lambda s: s or "null"
    if uoms is not None:
        for row in uoms:
            del row['id']
            del row['uom_name']
            del row['product_image']
            del row['synced_with_sap_status']
            del row['synced_with_sap_id']
            del row['synced_with_sap_time']
            del row['synced_with_sap_error_message']
            del row['created_at']
            del row['updated_at']

            row['height'] = xstr(row['height'])
            row['width'] = xstr(row['width'])
            row['length'] = xstr(row['length'])
            row['product_net_quantity_of_content'] = xstr(
                row['product_net_quantity_of_content'])
            row['product_image'] = ""

            row['upc'] = row['upc'] + str(product_id)
            product_uom.append(row)

    return product_uom


def remove_products(products_list: pd.DataFrame):
    '''Remove products with change name and uom'''
    products_list.short_name = ("[DEL]" + products_list.short_name.str[3:])
    products_list['basic_info'] = ''
    global PRODUCT_BASIC

    products_list['product_uom'] = products_list.id.apply(
        lambda x: api_requests.get_product_uom(x))

    for index, row in products_list.iterrows():
        row.product_uom = {"product_uoms":change_uom(row.product_uom, row.id)}
        # print(json.dumps(row.product_uom))

        basic_info = api_requests.get_products_basic(row.id)
        PRODUCT_BASIC['barcode_type'] = basic_info['barcode_type']
        PRODUCT_BASIC['brand_id'] = basic_info['brand_id']
        PRODUCT_BASIC['brand_type'] = basic_info['brand_type']
        PRODUCT_BASIC['country_code'] = basic_info['country_code']
        PRODUCT_BASIC['detail_name_en'] = basic_info['detail_name_en']
        PRODUCT_BASIC['detail_name'] = basic_info['detail_name']
        PRODUCT_BASIC['expiration_shelf_life_unit'] = basic_info['expiration_shelf_life_unit']
        PRODUCT_BASIC['final_approve'] = basic_info['final_approve']
        PRODUCT_BASIC['input_vat_id'] = basic_info['input_vat_id']
        PRODUCT_BASIC['manufacturer_id'] = basic_info['manufacturer_id']
        PRODUCT_BASIC['md_approve'] = basic_info['md_approve']
        PRODUCT_BASIC['minimum_display_quantity'] = basic_info['minimum_display_quantity']
        PRODUCT_BASIC['output_vat_id'] = basic_info['output_vat_id']
        PRODUCT_BASIC['preservation_temperature'] = basic_info['preservation_temperature']
        PRODUCT_BASIC['price_tag_issue_flag'] = basic_info['price_tag_issue_flag']
        PRODUCT_BASIC['product_specification'] = basic_info['product_specification']
        PRODUCT_BASIC['retail_business_type'] = basic_info['retail_business_type']
        PRODUCT_BASIC['sales_limitation'] = basic_info['sales_limitation']
        PRODUCT_BASIC['shipping_limitation'] = basic_info['shipping_limitation']
        PRODUCT_BASIC['short_name_en'] = basic_info['short_name_en']
        PRODUCT_BASIC['short_name'] = row.short_name
        PRODUCT_BASIC['total_shelf_life'] = basic_info['total_shelf_life']
        PRODUCT_BASIC['writeoffable'] = basic_info['writeoffable']
        PRODUCT_BASIC['sub_category_id'] = basic_info['sub_category_id']

        print(api_requests.put_product_basic(row.id, PRODUCT_BASIC).text)
        if row.product_uom is not None:
            print(api_requests.put_product_uom(row.id, json.loads(
                json.dumps(row.product_uom))).text)

    excel = pd.ExcelWriter('Removed_products_with_id.xlsx')
    products_list.to_excel(excel, 'temp')
    excel.save()


if __name__ == "__main__":
    file_path = os.path.abspath('Product_Master_JUN08.xlsx')
    # print(file_path)
    product_basic, uom, alter_uom, supplier, sale_area = product_master_new.read_product_master(
        file_path, 'Sheet1')

    update_success, failure = update_product_basic(product_basic)
    excel = pd.ExcelWriter('update_temp.xlsx')
    update_success.to_excel(excel, 'basic')
    excel.save()
    logger.debug(failure)

    # In case failed at update UOM, Open this Block
    rs_basic_info = pd.read_excel('update_temp.xlsx', 'basic')
    rs_basic_info = rs_basic_info[['short_name', 'id']]
    product_basic_uom_merged = pd.merge(rs_basic_info, uom, on='short_name')
    product_basic_alter_uom_merged = pd.merge(
        rs_basic_info,
        alter_uom,
        on='short_name')

    rs_uom_info, failures = product_master_new.put_product_uom(
        product_basic_uom_merged,
        product_basic_alter_uom_merged)
    rs_uom_info.to_excel(excel, 'uom')
    excel.save()
    logger.debug(failures)

    # In case failed at update supplier, Open this block
    rs_uom_info = pd.DataFrame(
        pd.read_excel('update_temp.xlsx', 'uom'))
    # print(rs_uom_info)
    product_basic_supplier_merged = pd.merge(
        rs_uom_info, supplier,
        left_on='product_name',
        right_on='short_name')
    rs_product_supplier, failures = product_master_new.put_product_supplier(
        product_basic_supplier_merged)
    rs_product_supplier.to_excel(excel, 'supplier')
    excel.save()
    logger.debug(failures)

    # In case failed update retail price, Open this block
    rs_product_supplier = pd.read_excel('update_temp.xlsx', 'uom')
    rs_product_supplier = rs_product_supplier[['product_name', 'product_id']]
    product_retail_price_merged = pd.merge(
        rs_product_supplier,
        sale_area, left_on='product_name',
        right_on='short_name')
    # print(product_retail_price_merged)
    rs_product_retail_price, failures = product_master_new.put_product_retail_price(
        product_retail_price_merged)
    rs_product_retail_price.to_excel(excel, 'retail')
    excel.save()
    logger.debug(failures)

    # products_list = get_products_id('./check_products_id.xlsx')
    # remove_products(products_list)
