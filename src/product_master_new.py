import json
import datetime
import time
import re
import ast
import pandas as pd
import numpy as np
import logging
import difflib
import api_requests
from unidecode import unidecode
from logging.config import fileConfig

fileConfig('logging_config.ini')
logger = logging.getLogger()

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
TOMORROW = int(time.mktime((datetime.datetime.today() +
                            datetime.timedelta(days=1)).timetuple()) * 1000)
FULFILLMENT = {'CDC': 0, 'DSD': 1}
INVENTORY_TYPE = {'Stock': 0, 'Pass': 1}
SORTING_TYPE = {'CDC sorting': 0}
YES_NO = {'Yes': True, 'No': False}
APPROVED = {'Approved': True, 'Reject': False, 'Review': False}
TEMPERATURE = {'Ambient': 0, 'Air-condition': 1, 'Frozen': 2, 'Chilled': 3}
BARCODE_ISSUER = {'Manufacture': 0, 'HQ': 1}
SHELF_LIFE = {'hour': 0, 'day': 1, 'month': 2, 'nan': 3}
BRAND_TYPE = {'Private': 0, 'National': 1, 'Multinational': 2}
PRODUCT_CONTENT = {'gram': 'g'}
BUSINESS_TYPE = {
    'Normal Merchandise': 0,
    'Food Service non BOM': 1,
    'Food Service BOM': 2,
    'Consumable supplies': 3,
    'Materials': 4,
    'Consignment': 5,
    'Gift/Sell Voucher': 6,
    'Service w/o Cost': 7}
BASIC_INFO = [
    'short_name',
    'retail_business_type',
    'detail_name',
    'short_name_en',
    'detail_name_en',
    'sub_category_id',
    'preservation_temperature',
    'writeoffable',
    'price_tag_issue_flag',
    'product_specification',
    'country_code',
    'barcode_type',
    'manufacturer_id',
    # 'brand_type',
    'brand_id',
    'minimum_display_quantity',
    'expiration_shelf_life_unit',
    'total_shelf_life',
    'shipping_limitation',
    'sales_limitation',
    'input_vat_id',
    'output_vat_id',
    'md_approve',
    'final_approve',
    'commission_type',
    'commission_value']
PRODUCT_UOM = [
    'upc',
    'uom',
    'uom_size',
    'base_unit',
    'height',
    'width',
    'length',
    'product_content_unit',
    'product_net_quantity_of_content',
    'product_image'
]
PRODUCT_SUPPLIER = [
    'sale_area_id',
    'supplier_id',
    'core_item',
    'fulfillment_method',
    'inventory_type',
    'sorting_type',
    # 'new_purchase_price_with_tax',
    'new_purchase_price_without_tax',
    'new_purchase_price_effective_date',
    'first_order_date',
    'end_order_date',
    'supplier_product_id',
    'minimum_order_quantity',
    'maximum_order_quantity',
    'returnable',
    'store_orderable'
]
PRODUCT_SALE_AREA = [
    'product_vendor_mapping_id',
    'product_uom_id',
    'retail_selling_price_with_tax',
    # 'retail_selling_price_without_tax',
    'retail_selling_price_effective_date',
    'new_retail_selling_price_with_tax',
    # 'new_retail_selling_price_without_tax',
    'new_retail_selling_price_effective_date'
]


def read_product_master(excel_file, sheet_name=None, header=0):
    product_master = pd.DataFrame(pd.read_excel(
        excel_file,
        sheetname=sheet_name,
        header=header,
        converters={
            'UPC Base Unit - Cannot Edit': str,
            'Alternative UOM UPC': str}))
    product_master = product_master.drop_duplicates(
        'Product short name VN - 26 Char Limit')
    # product_master = product_master[np.isfinite(product_master['Product short name VN - 26 Char Limit'])]

    # Create df for each tab info
    product_basic_df = pd.DataFrame(columns=BASIC_INFO)
    product_uom_df = pd.DataFrame(columns=PRODUCT_UOM)
    alternative_uom = pd.DataFrame(columns=PRODUCT_UOM)
    product_supplier_df = pd.DataFrame(columns=PRODUCT_SUPPLIER)
    product_sale_area_df = pd.DataFrame(columns=PRODUCT_SALE_AREA)

    # Mapping product info
    product_basic_df['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    product_basic_df['retail_business_type'] = product_master['Retail Business Type - Cannot Edit']
    product_basic_df['detail_name'] = product_master['Product full name VN (default)'].str.strip(
    )
    product_basic_df['short_name_en'] = product_master['Product short name EN']
    product_basic_df['detail_name_en'] = product_master['Product full name EN']
    product_basic_df['sub_category_id'] = product_master['Product Sub-category - Cannot Edit'].str.strip()
    product_basic_df['sub_category_id'] = product_basic_df['sub_category_id'].str.lower()
    product_basic_df['sub_category_id'] = product_basic_df['sub_category_id'].apply(
        lambda x: unidecode(x))
    product_basic_df['preservation_temperature'] = product_master['Preservation\ntemperature']
    product_basic_df['writeoffable'] = product_master['Write-off-able Flag']
    product_basic_df['price_tag_issue_flag'] = product_master['Price tag issue Flag']
    product_basic_df['product_specification'] = product_master['Product Spec Content']
    product_basic_df['country_code'] = product_master['Country of origin']
    product_basic_df['barcode_type'] = product_master['Barcode issuer']
    product_basic_df['manufacturer_id'] = product_master['Manufacturer Name'].str.strip()
    product_basic_df['manufacturer_id'] = product_basic_df['manufacturer_id'].str.lower()
    product_basic_df['manufacturer_id'] = product_basic_df['manufacturer_id'].apply(
        lambda x: unidecode(x))
    # product_basic_df['brand_type']                 = product_master['Brand Type']
    product_basic_df['brand_id'] = product_master['Brand Name'].astype(str)
    product_basic_df['brand_id'] = product_master['Brand Name'].str.strip()
    product_basic_df['brand_id'] = product_basic_df['brand_id'].str.lower()
    # product_basic_df['brand_id']                   = product_basic_df['brand_id'].apply(lambda x: unidecode(x))
    product_basic_df['minimum_display_quantity'] = product_master['Minimum Display Qty']
    product_basic_df['expiration_shelf_life_unit'] = product_master['Expiration Shelf life unit'].astype(str).str.lower(
    ).str.strip()
    product_basic_df['total_shelf_life'] = product_master['Total shelf life']
    product_basic_df['shipping_limitation'] = product_master['Delivery limitation'] * 100
    product_basic_df['sales_limitation'] = product_master['Sales limitation'] * 100
    product_basic_df['input_vat_id'] = product_master['Input VAT code description']
    product_basic_df['output_vat_id'] = product_master['Output VAT code description']
    product_basic_df['md_approve'] = False
    product_basic_df['final_approve'] = False
    product_basic_df['commission_type'] = ''
    product_basic_df['commission_value'] = ''

    # Mapping product PRODUCT_UOM
    product_uom_df['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    # product_uom_df['upc']                              = product_master['UPC Base Unit - Cannot Edit'].astype(float)
    product_uom_df['upc'] = product_master['UPC Base Unit - Cannot Edit']
    # print(product_uom_df['upc'].astype(str)[:-2])
    # print(product_uom_df['upc'])
    product_uom_df['uom'] = product_master['UoM Code - Cannot Edit'].astype(
        str)
    product_uom_df['uom_size'] = product_master['UoM size - Cannot Edit'].astype(
        int)
    product_uom_df['base_unit'] = True
    product_uom_df['height'] = ''
    product_uom_df['width'] = ''
    product_uom_df['length'] = ''
    product_uom_df['product_content_unit'] = 'g'
    product_uom_df['product_net_quantity_of_content'] = ''
    product_uom_df['product_image'] = ''

    alternative_uom['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    # alternative_uom['upc']                             = product_master['Alternative UOM UPC'].astype(float)
    alternative_uom['upc'] = product_master['Alternative UOM UPC']
    alternative_uom['uom'] = product_master['Alternative UOM code'].astype(str)
    alternative_uom['uom_size'] = product_master['Alternative UoM size']
    alternative_uom['base_unit'] = False
    alternative_uom['height'] = ''
    alternative_uom['width'] = ''
    alternative_uom['length'] = ''
    alternative_uom['product_content_unit'] = 'g'
    alternative_uom['product_net_quantity_of_content'] = ''
    alternative_uom['product_image'] = ''

    # Mapping product supplier
    product_supplier_df['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    product_supplier_df['sale_area_id'] = 1
    product_supplier_df['supplier_id'] = product_master['Supplier Name'].str.lower(
    ).str.strip()
    product_supplier_df['supplier_id'] = product_supplier_df['supplier_id'].apply(
        lambda x: unidecode(x))
    product_supplier_df['core_item'] = product_master['Core item Flag']
    product_supplier_df['fulfillment_method'] = product_master['Fulfillment Method']
    product_supplier_df['inventory_type'] = product_master['Inventory type']
    product_supplier_df['sorting_type'] = product_master['Sorting type']
    # product_supplier_df['new_purchase_price_with_tax']       = None
    product_supplier_df['new_purchase_price_without_tax'] = product_master['Purchase Price (-VAT)'].round(
        0)
    # print(product_supplier_df['new_purchase_price_without_tax'])
    product_supplier_df['new_purchase_price_effective_date'] = TOMORROW
    product_supplier_df['first_order_date'] = TOMORROW
    product_supplier_df['end_order_date'] = None
    product_supplier_df['supplier_product_id'] = ""
    product_supplier_df['minimum_order_quantity'] = product_master['MOQ']
    product_supplier_df['maximum_order_quantity'] = product_master['MaxOQ']
    product_supplier_df['returnable'] = product_master['Store Return-able Flag']
    product_supplier_df['store_orderable'] = product_master['Store Order-able Flag']
    product_supplier_df['logistics_id'] = product_master['Logistics group'].str.strip(
    )
    # product_supplier_df['logistics_id']                      = product_supplier_df['logistics_id'].apply(lambda x: unidecode(x))
    product_supplier_df['refill_uom_id'] = product_master['Refill UoM']
    product_supplier_df['store_uom_id'] = product_master['Store Order UoM']

    # Mapping product sale area
    product_sale_area_df['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    # product_sale_area_df['product_vendor_mapping_id']             = product_master['']
    # product_sale_area_df['product_uom_id']                        = product_master['']
    product_sale_area_df['retail_selling_price_with_tax'] = None
    # product_sale_area_df['retail_selling_price_without_tax']      = None
    product_sale_area_df['retail_selling_price_effective_date'] = None
    product_sale_area_df['new_retail_selling_price_with_tax'] = product_master['Retail Price (+VAT)'].round(
        0)
    # print(product_sale_area_df['new_retail_selling_price_with_tax'])
    # product_sale_area_df['new_retail_selling_price_without_tax']  = product_master['']
    product_sale_area_df['new_retail_selling_price_effective_date'] = TOMORROW

    # Replace value with valid value
    sub_category_id = api_requests.get_sub_category()
    sub_category_id = pd.DataFrame(sub_category_id)
    # print(sub_category_id)
    sub_category_id['name'] = sub_category_id['name'].str.strip()
    sub_category_id = sub_category_id[['name', 'id']].set_index('name')
    sub_category_id = sub_category_id.to_dict()['id']
    sub_category_id = ast.literal_eval(unidecode(str(sub_category_id).lower()))

    brand_id = api_requests.get_product_brands()
    brand_id = pd.DataFrame(brand_id)
    brand_id['name'] = brand_id.name.str.strip()

    brand_id['name'] = brand_id.name.str.lower()
    brand_type = pd.DataFrame(brand_id[['name', 'brand_type']])
    brand_type['name'] = brand_type.name.str.strip()
    brand_type['name'] = brand_type.name.str.lower()
    # brand_type['name'] = brand_type['name'].apply(lambda x: unidecode(x))
    # product_basic_df['brand_id'] = product_basic_df['brand_id'].apply(lambda x: unidecode(x))
    # product_basic_df['brand_id'] = product_basic_df['brand_id'].apply(lambda x: difflib.get_close_matches(x, brand_type['name']))
    # logger.debug(product_basic_df['brand_id'])

    brand_id = brand_id[['name', 'id']].set_index('name')
    brand_id = brand_id.to_dict()['id']
    # brand_id         = ast.literal_eval(unidecode(str(brand_id)))

    manufacturers_id = api_requests.get_product_manufacturers()
    manufacturers_id = pd.DataFrame(manufacturers_id)
    manufacturers_df = pd.DataFrame(manufacturers_id)
    manufacturers_id['name'] = manufacturers_id.name.str.lower()
    manufacturers_id['name'] = manufacturers_id.name.str.strip()
    manufacturers_id = manufacturers_id[['name', 'id']].set_index('name')
    manufacturers_id = manufacturers_id.to_dict()['id']
    manufacturers_id = ast.literal_eval(unidecode(str(manufacturers_id)))
    # logger.debug(manufacturers_id)

    inputvat = api_requests.get_input_vat()
    inputvat = pd.DataFrame(inputvat)
    inputvat = inputvat[['description', 'id']].set_index('description')
    inputvat = inputvat.to_dict()['id']

    outputvat = api_requests.get_output_vat()
    outputvat = pd.DataFrame(outputvat)
    outputvat = outputvat[['description', 'id']].set_index('description')
    outputvat = outputvat.to_dict()['id']

    supplier_id = api_requests.get_suppliers()
    supplier_id = pd.DataFrame(supplier_id)
    supplier_id = supplier_id[['name', 'id']].set_index('name')
    supplier_id = supplier_id.to_dict()['id']
    supplier_id = ast.literal_eval(unidecode(str(supplier_id).lower()))

    logistic_group = api_requests.get_logistics_group()
    logistic_group = pd.DataFrame(logistic_group)
    product_supplier_df['logistics_id'] = product_supplier_df['logistics_id'].apply(
        lambda x: difflib.get_close_matches(x, logistic_group['name'])[0])
    # logger.debug(product_supplier_df['logistics_id'])

    logistic_group = logistic_group[['name', 'id']].set_index('name')
    logistic_group = logistic_group.to_dict()['id']
    # logistic_group = ast.literal_eval(unidecode(str(logistic_group).lower()))

    # Join table to get brand type
    product_basic_merged_df = pd.merge(
        product_basic_df, brand_type, how='left', left_on='brand_id', right_on='name')
    del product_basic_merged_df['name']
    product_basic__cannot_merged_df = product_basic_merged_df[pd.isnull(
        product_basic_merged_df.brand_type)]
    product_basic__cannot_merged_df.to_csv('product_cannot_merged_brand.csv')
    product_basic_merged_df = product_basic_merged_df[pd.notnull(
        product_basic_merged_df.brand_type)]
    # product_basic_merged_df = product_basic_df.join(brand_type, on='brand_id', how='left')

    product_basic_merged_df = product_basic_merged_df.replace(
        {'sub_category_id': sub_category_id})
    product_basic_merged_df = product_basic_merged_df.replace(
        {'preservation_temperature': TEMPERATURE}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'writeoffable': YES_NO}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'price_tag_issue_flag': YES_NO}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'retail_business_type': BUSINESS_TYPE}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'barcode_type': BARCODE_ISSUER}, regex=True)
    # product_basic_merged_df = product_basic_merged_df.replace({'brand_type': BRAND_TYPE}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'brand_id': brand_id})

    # product_basic_merged_df['manufacturer_id'] = product_basic_merged_df['manufacturer_id'].apply(lambda x: difflib.get_close_matches(x, manufacturers_df['name'].str.lower().str.strip())[0])
    # product_basic_merged_df['manufacturer_id'] = product_basic_merged_df['manufacturer_id'].str.lower().str.strip()
    # product_basic_merged_df['manufacturer_id'] = product_basic_merged_df['manufacturer_id'].apply(lambda x: unidecode(x))
    # manufacturers_df['name'] = manufacturers_df['name'].str.lower().str.strip()
    # manufacturers_df['name'] = manufacturers_df['name'].apply(lambda x: unidecode(x))
    # product_basic_merged_df['manufacturer_id'] = product_basic_merged_df['manufacturer_id'].apply(lambda x: difflib.get_close_matches(x, manufacturers_df['name'])[0])
    # logger.debug(product_basic_merged_df['manufacturer_id'])
    # product_basic_merged_df['manufacturer_id'].to_csv('temp_manufacturer.csv')

    product_basic_merged_df = product_basic_merged_df.replace(
        {'manufacturer_id': manufacturers_id})
    product_basic_merged_df = product_basic_merged_df.replace(
        {'expiration_shelf_life_unit': SHELF_LIFE})
    product_basic_merged_df['expiration_shelf_life_unit'].fillna(
        value=3, inplace=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'input_vat_id': inputvat}, regex=True)
    product_basic_merged_df = product_basic_merged_df.replace(
        {'output_vat_id': outputvat}, regex=True)
    # product_basic_merged_df = product_basic_merged_df.replace({'md_approve': APPROVED}, regex=True)
    # product_basic_merged_df = product_basic_merged_df.replace({'final_approve': APPROVED}, regex=True)
    # product_uom_df = product_uom_df.replace({'product_net_quantity_of_content': PRODUCT_CONTENT}, regex=True)

    # Replacing value for product supplier
    product_supplier_df = product_supplier_df.replace(
        {'fulfillment_method': FULFILLMENT})
    product_supplier_df = product_supplier_df.replace(
        {'inventory_type': INVENTORY_TYPE})
    product_supplier_df = product_supplier_df.replace(
        {'sorting_type': SORTING_TYPE})
    product_supplier_df = product_supplier_df.replace({'core_item': YES_NO})
    product_supplier_df = product_supplier_df.replace(
        {'store_orderable': YES_NO})
    product_supplier_df = product_supplier_df.replace({'returnable': YES_NO})
    product_supplier_df = product_supplier_df.replace(
        {'supplier_id': supplier_id})
    product_supplier_df = product_supplier_df.replace(
        {'logistics_id': logistic_group})

    product_basic_merged_df = product_basic_merged_df.drop_duplicates(
        'short_name')
    product_uom_df = product_uom_df.drop_duplicates('short_name')
    alternative_uom = alternative_uom.drop_duplicates('short_name')
    product_supplier_df = product_supplier_df.drop_duplicates('short_name')
    product_sale_area_df = product_sale_area_df.drop_duplicates('short_name')

    return product_basic_merged_df, product_uom_df, alternative_uom, product_supplier_df, product_sale_area_df


def mapping_alternative_uom(excel_file):
    product_master = pd.DataFrame(pd.read_excel(excel_file))
    product_master = product_master.drop_duplicates(
        'Product short name VN - 26 Char Limit')

    alternative_uom = pd.DataFrame(columns=PRODUCT_UOM)

    alternative_uom['short_name'] = product_master['Product short name VN - 26 Char Limit'].str.strip()
    # alternative_uom['upc']                             = product_master['Alternative UOM UPC'].astype(str)
    alternative_uom['upc'] = product_master['Alternative UOM UPC']
    alternative_uom['uom'] = product_master['Alternative UOM code'].astype(str)
    alternative_uom['uom_size'] = product_master['Alternative UoM size'].astype(
        int)
    alternative_uom['base_unit'] = False
    alternative_uom['height'] = ''
    alternative_uom['width'] = ''
    alternative_uom['length'] = ''
    alternative_uom['product_content_unit'] = 'g'
    alternative_uom['product_net_quantity_of_content'] = ''
    alternative_uom['product_image'] = ''


def post_product_basic(product_basic_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    product_basic_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in product_basic_df.iterrows():
        basic_info = json.loads(row.to_json())
        response = api_requests.post_product_basic(basic_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(basic_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def put_product_uom(product_uom_df: pd.DataFrame, product_alter_uom_df: pd.DataFrame):
    product_uom_df = product_uom_df.sort_values(by='id', ascending=True)
    product_uom_df = product_uom_df.reset_index(drop=True)
    product_alter_uom_df = product_alter_uom_df.sort_values(
        by='id', ascending=True)
    product_alter_uom_df = product_alter_uom_df.reset_index(drop=True)
    del product_alter_uom_df['id']
    del product_alter_uom_df['short_name']
    response_success = list()
    response_failed = list()
    product_uom_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in product_uom_df.iterrows():
        uom_info = json.loads(row.to_json())
        alter_info = product_alter_uom_df.iloc[[index]].to_dict(orient="index")
        # print(alter_info)
        # uom_info['upc'] = str(uom_info['upc'])[:-2]
        # alter_info[index]['upc'] = str(alter_info[index]['upc'])[:-2]
        product_id = uom_info['id']
        del uom_info['short_name']
        del uom_info['id']
        if alter_info[index]['uom'] == 'nan':
            uom_info = {"product_uoms": [uom_info]}
        else:
            uom_info = {"product_uoms": [uom_info, alter_info[index]]}
        # uom_info = {"product_uoms": [uom_info, alter_info[index]]}
        # print(product_id)
        logger.debug(product_id)
        # print(uom_info)
        logger.debug(uom_info)
        response = api_requests.put_product_uom(
            product_id,
            uom_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(uom_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def put_product_supplier(product_supplier_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    product_supplier_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()

    product_supplier_first = pd.DataFrame(product_supplier_df[[
        'product_id',
        'logistics_id',
        'refill_uom_id',
        'store_uom_id'
    ]])
    product_supplier_last = pd.DataFrame(product_supplier_df[[
        'product_id',
        'sale_area_id',
        'supplier_id',
        'core_item',
        'fulfillment_method',
        'inventory_type',
        'sorting_type',
        # 'new_purchase_price_with_tax',
        'new_purchase_price_without_tax',
        'new_purchase_price_effective_date',
        'first_order_date',
        'end_order_date',
        'supplier_product_id',
        'minimum_order_quantity',
        'maximum_order_quantity',
        'returnable',
        'store_orderable'
    ]])
    product_supplier_last['end_order_date'] = None
    product_supplier_last['new_purchase_price_without_tax'] = product_supplier_last[
        'new_purchase_price_without_tax'].astype(str)
    product_supplier_first = product_supplier_first.sort_values(
        by='product_id', ascending=True)
    product_supplier_first = product_supplier_first.reset_index(drop=True)
    product_supplier_last = product_supplier_last.sort_values(
        by='product_id', ascending=True)
    product_supplier_last = product_supplier_last.reset_index(drop=True)
    del product_supplier_last['product_id']

    for index, row in product_supplier_first.iterrows():
        product_supplier_info = json.loads(row.to_json())
        product_id = product_supplier_info['product_id']

        product_uom = api_requests.get_product_uom(int(product_id))
        product_uom = pd.DataFrame(product_uom)
        product_uom = product_uom[['uom', 'id']].set_index('uom')
        product_uom = product_uom.to_dict()['id']
        del product_supplier_info['product_id']

        product_supplier_info = {k: product_uom.get(
            v, v) for k, v in product_supplier_info.items()}
        product_supplier_area = product_supplier_last.iloc[[
            index]].to_dict(orient="index")
        product_supplier_area = {
            "supplier_mapping_info": [product_supplier_area[index]]}
        # product_supplier_info = product_supplier_info.copy()
        product_supplier_info.update(product_supplier_area)
        # print(product_supplier_info)
        logger.debug(product_supplier_info)
        product_supplier_info = ast.literal_eval(str(product_supplier_info))
        # print(product_id)
        logger.debug(product_id)
        # print(product_supplier_area)
        # print(type(product_supplier_info))

        response = api_requests.put_product_supplier(
            product_id, product_supplier_info)
        # print(response.json())
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(product_supplier_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def put_product_retail_price(product_retail_price_df: pd.DataFrame):
    # product_retail_price_df = product_retail_price_df.sort_values(by='product_id', ascending=True)
    # product_retail_price_df = product_retail_price_df.reset_index(drop=True)
    response_success = list()
    response_failed = list()
    product_retail_price_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in product_retail_price_df.iterrows():
        retail_price_info = json.loads(row.to_json())
        product_id = retail_price_info['product_id']
        del retail_price_info['product_id']
        del retail_price_info['product_name']
        del retail_price_info['short_name']

        retail_price_detail = api_requests.get_product_retail_price(product_id)
        # retail_price_detail = pd.DataFrame(retail_price_detail)
        # retail_price_detail = retail_price_detail.set_index('product_id')
        retail_price_detail = list(
            retail_price_detail['retail_price_sale_area_mapping_details'])
        retail_price_detail_first = retail_price_detail[0]

        retail_price_info_first = retail_price_info.copy()
        retail_price_info_first['product_vendor_mapping_id'] = retail_price_detail_first['product_vendor_mapping_id']
        retail_price_info_first['product_uom_id'] = retail_price_detail_first['product_uom_id']
        retail_price_info_first['new_retail_selling_price_with_tax'] = retail_price_info[
            'new_retail_selling_price_with_tax'] * retail_price_detail_first['uom_size']
        print(str(product_id) + " - " + str(retail_price_detail))
        if len(retail_price_detail) > 1:
            retail_price_detail_second = retail_price_detail[1]
            retail_price_info_second = retail_price_info.copy()
            retail_price_info_second['product_vendor_mapping_id'] = retail_price_detail_second['product_vendor_mapping_id']
            retail_price_info_second['product_uom_id'] = retail_price_detail_second['product_uom_id']
            retail_price_info_second['new_retail_selling_price_with_tax'] = retail_price_info[
                'new_retail_selling_price_with_tax'] * retail_price_detail_second['uom_size']
            retail_price_info = {
                "apply_all_sale_area": True,
                "retail_price_sale_area_mapping_form_details": [retail_price_info_first, retail_price_info_second]}
        else:
            retail_price_info = {
                "apply_all_sale_area": True,
                "retail_price_sale_area_mapping_form_details": [retail_price_info_first]}

        # print(retail_price_info)
        logger.debug(retail_price_info)
        response = api_requests.put_product_retail_price(
            product_id,
            retail_price_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(retail_price_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed
