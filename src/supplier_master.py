import json
import datetime
import time
import re
import ast
import pandas as pd
import api_requests
import convert_vn

TODAY = int(time.mktime(datetime.datetime.today().timetuple()) * 1000)
CLASSIFICATION = {'ZV01_Merchandise Supplier': 0}
COMPANY_TYPE = {'Wholesaler': 0, 'Manufacture': 1, 'Other': 2}
DATE = {'Mon': 0, 'Tue': 1, 'Wed': 2, 'Thu': 3, 'Fri': 4, 'Sat': 5, 'Sun': 6}
BASIC_INFO = [
    'name',
    'classification',
    'supplier_branch_id',
    'company_type',
    'name_en',
    'creation_date',
    'tax_code',
    'tel_number1',
    'tel_number2',
    'fax_number',
    'address_line1',
    'address_line2',
    'city_id',
    'district_id',
    'ward_id',
    'postal_address'
]
CONTACT_INFO = [
    'name',
    'first_person_name',
    'first_person_email',
    'first_person_phone',
    'second_person_name',
    'second_person_email',
    'second_person_phone',
    'accounting_person_name',
    'accounting_person_email',
    'accounting_person_phone'
]
TRADING_INFO = [
    'contract_ref_no',
    'contract_start_date',
    'contract_end_date',
    'contract_extension_period_day',
    'minimum_order',
    'order_type',
    'price_changing_inform_day',
    'discount_on_invoice',
    'cdc_allowance',
    'rebate',
    'other_support_type',
    'other_support_description']
PAYMENT_INFO = [
    'name',
    'payment_term_id',
    'invoice_sending_date',
    'reconsolidation_date',
    'bank_key',
    'bank_street',
    'bank_city',
    'bank_account_name',
    'bank_account_no',
    'bank_name',
    'bank_branch',
    'bank_country',
    'swift_code'
]


def read_supplier_master(excel_file):
    supplier_df = pd.DataFrame(pd.read_excel(
        excel_file,
        converters={
            'Tel number': str,
            'Phone No1': str,
            'Phone No2': str,
            'Phone No3': str,
            'Payment term': str,
            'Consolidation date': str
        }))
    supplier_df = supplier_df.drop_duplicates('Supplier name Việt Nam')

    # Mapping suppliers info
    supplier_json = pd.DataFrame(columns=BASIC_INFO)
    supplier_json['name'] = supplier_df['Supplier name Việt Nam'].str.strip()
    supplier_json['classification'] = supplier_df['Supplier classification name']
    supplier_json['supplier_branch_id'] = supplier_df['Supplier branch name']
    supplier_json['company_type'] = supplier_df['Company type']
    supplier_json['name_en'] = supplier_df['Supplier name ENG']
    supplier_json['creation_date'] = TODAY
    supplier_json['tax_code'] = supplier_df['Supplier tax code']
    supplier_json['tel_number1'] = supplier_df['Tel number'].str[:30]
    # supplier_json['tel_number2']        = supplier_df['Mobile phone'].str[:30]
    supplier_json['tel_number2'] = supplier_df['Mobile phone'].astype(
        str).str[:30]
    supplier_json['fax_number'] = supplier_df['Fax number']
    supplier_json['address_line1'] = supplier_df['Supplier Legal Address line 1']
    supplier_json['address_line2'] = supplier_df['Supplier Legal Address line 2']
    supplier_json['city_id'] = supplier_df['Supplier Legal address City']
    supplier_json['district_id'] = supplier_df['Supplier Legal address District']
    supplier_json['ward_id'] = supplier_df['Supplier Legal address Ward']
    supplier_json['postal_address'] = supplier_df['Supplier Postal address']

    # Mapping Contact info
    sp_contact_json = pd.DataFrame(columns=CONTACT_INFO)
    sp_contact_json['name'] = supplier_df['Supplier name Việt Nam'].str.strip()
    sp_contact_json['first_person_name'] = supplier_df['Contact Person 1 Name']
    sp_contact_json['first_person_email'] = supplier_df['Email 1']
    sp_contact_json['first_person_phone'] = supplier_df['Phone No1'].str[:16]
    sp_contact_json['second_person_name'] = supplier_df['Contact Person 2 Name']
    sp_contact_json['second_person_email'] = supplier_df['Email 2']
    sp_contact_json['second_person_phone'] = supplier_df['Phone No2'].str[:16]
    sp_contact_json['accounting_person_name'] = supplier_df['Accounting Contact Name']
    sp_contact_json['accounting_person_email'] = supplier_df['Email 3']
    sp_contact_json['accounting_person_phone'] = supplier_df['Phone No3'].str[:16]

    # Mapping Payment info
    sp_payment_json = pd.DataFrame(columns=PAYMENT_INFO)
    sp_payment_json['name'] = supplier_df['Supplier name Việt Nam'].str.strip()
    sp_payment_json['payment_term_id'] = supplier_df['Payment term'].str[:4]
    sp_payment_json['invoice_sending_date'] = supplier_df['Invoice sending date']
    sp_payment_json['reconsolidation_date'] = supplier_df['Consolidation date'].str[:2]
    sp_payment_json['bank_key'] = supplier_df['Bank key']
    sp_payment_json['bank_street'] = supplier_df['Bank Street']
    sp_payment_json['bank_city'] = supplier_df['Bank City']
    sp_payment_json['bank_account_name'] = supplier_df['Bank account Name']
    sp_payment_json['bank_account_no'] = supplier_df['Bank account No.']
    sp_payment_json['bank_name'] = supplier_df['Bank name']
    sp_payment_json['bank_branch'] = supplier_df['Bank Branch']
    sp_payment_json['bank_country'] = supplier_df['Bank Country']
    sp_payment_json['swift_code'] = supplier_df['Swift code/ IBAN']

    # Mapping Trading info
    sp_trading_json = pd.DataFrame(columns=TRADING_INFO)
    sp_trading_json['name'] = supplier_df['Supplier name Việt Nam'].str.strip()
    sp_trading_json['contract_ref_no'] = ""
    sp_trading_json['contract_start_date'] = ""
    sp_trading_json['contract_end_date'] = ""
    sp_trading_json['contract_extension_period_day'] = ""
    sp_trading_json['minimum_order'] = ""
    sp_trading_json['order_type'] = 0
    sp_trading_json['price_changing_inform_day'] = ""
    sp_trading_json['discount_on_invoice'] = 0
    sp_trading_json['cdc_allowance'] = 0
    sp_trading_json['rebate'] = 0
    sp_trading_json['other_support_type'] = 0
    sp_trading_json['other_support_description'] = ""

    # Replace some info to match with system
    supplier_json = supplier_json.replace(
        {'classification': CLASSIFICATION}, regex=True)

    brands = api_requests.get_brands()
    brands_df = pd.DataFrame(json.loads(brands))
    brands_df = brands_df[['name', 'id']].set_index('name')
    brands_df = brands_df.to_dict()['id']
    supplier_json = supplier_json.replace(
        {'supplier_branch_id': brands_df}, regex=True)

    cities = api_requests.get_cities()
    cities_df = pd.DataFrame(json.loads(cities))
    cities_df = cities_df[['name', 'id']].set_index('name')
    cities_df = cities_df.to_dict()['id']
    cities_df = ast.literal_eval(convert_vn.convert(str(cities_df)))

    districts = api_requests.get_districts()
    districts_df = pd.DataFrame(json.loads(districts))
    districts_df = districts_df[['name', 'id']].set_index('name')
    districts_df = districts_df.to_dict()['id']
    districts_df = ast.literal_eval(convert_vn.convert(str(districts_df)))

    wards = api_requests.get_wards()
    wards_df = pd.DataFrame(json.loads(wards))
    wards_df = wards_df[['name', 'id']].set_index('name')
    wards_df = wards_df.to_dict()['id']
    wards_df = ast.literal_eval(convert_vn.convert(str(wards_df)))

    for index, row in supplier_json.iterrows():
        row['city_id'] = convert_vn.convert(row['city_id']).strip()
        row['district_id'] = convert_vn.convert(
            str(row['district_id'])).strip()
        row['ward_id'] = convert_vn.convert(str(row['ward_id'])).strip()

        supplier_json.set_value(index, 'city_id', row['city_id'])
        supplier_json.set_value(index, 'district_id', row['district_id'])
        supplier_json.set_value(index, 'ward_id', row['ward_id'])
    supplier_json = supplier_json.replace({'city_id': cities_df}, regex=True)
    supplier_json = supplier_json.replace({'district_id': districts_df})
    supplier_json = supplier_json.replace({'ward_id': wards_df})

    supplier_json = supplier_json.replace(
        {'company_type': COMPANY_TYPE}, regex=True)
    supplier_json = supplier_json.drop_duplicates('name')

    sp_payment_json = sp_payment_json.replace({'invoice_sending_date': DATE})
    return supplier_json, sp_contact_json, sp_payment_json, sp_trading_json


def post_supplier_basic(supplier_basic_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    supplier_basic_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in supplier_basic_df.iterrows():
        basic_info = json.loads(row.to_json())
        response = api_requests.post_supplier(basic_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(basic_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def put_supplier_basic(supplier_basic_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    supplier_basic_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in supplier_basic_df.iterrows():
        basic_info = json.loads(row.to_json())
        supplier_id = int(basic_info['id'])
        supplier_basic = api_requests.get_supplier_basic()
        response = api_requests.post_supplier(basic_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(basic_info)
    response_success_df = pd.DataFrame(
        pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def put_supplier_contact(supplier_contact_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    supplier_contact_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in supplier_contact_df.iterrows():
        contact_info = json.loads(row.to_json())
        del contact_info['name']
        response = api_requests.put_supplier_contact(
            int(row['id']), contact_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(contact_info)
    response_success_df = pd.DataFrame(eval(json.dumps(response_success)))
    return response_success_df, response_failed


def put_supplier_trading(supplier_trading_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    supplier_trading_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in supplier_trading_df.iterrows():
        trading_info = json.loads(row.to_json())
        del trading_info['name']
        response = api_requests.put_supplier_trading(
            int(row['id']), trading_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(trading_info)
    # response_success_df = pd.DataFrame(eval(json.dumps(response_success)))
    return response_success_df, response_failed


def put_supplier_payment(supplier_payment_df: pd.DataFrame):
    response_success = list()
    response_failed = list()
    supplier_payment_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in supplier_payment_df.iterrows():
        payment_info = json.loads(row.to_json())
        del payment_info['name']
        del payment_info['supplier_name']
        response = api_requests.put_supplier_payment(
            int(row['supplier_id']), payment_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(payment_info)
    response_success_df = pd.DataFrame(eval(json.dumps(response_success)))
    return response_success_df, response_failed


# a, b, c, d = read_supplier_master('supplier_master.xlsx')
# # print(a)
# excel = pd.ExcelWriter('supplier_output_.xlsx')
# a.to_excel(excel, 'supplier')
# b.to_excel(excel, 'contact')
# c.to_excel(excel, 'payment')
# d.to_excel(excel, 'trading')
# excel.save()

a = pd.DataFrame(pd.read_excel('supplier_output_.xlsx', 'supplier'))
result_basic, failures = post_supplier_basic(a)
print(failures)
result_basic.to_excel(excel, "supplier_id")
excel.save()

# # put supplier contact info
# # result_basic = pd.DataFrame(pd.read_excel('supplier_output.xlsx', "supplier_id"))
# result_basic = result_basic[['name', 'id']]
# supplier_contact_merged = pd.merge(result_basic, b, on='name')
# # print(supplier_contact_merged)
# supplier_contact_info, failures = put_supplier_contact(supplier_contact_merged)
# supplier_contact_info.to_excel(excel, "supplier_contact")
# excel.save()
# print(failures)

# # Put supplier trading info
# result_basic = result_basic[['name', 'id']]
# supplier_trading_merged = pd.merge(result_basic, d, on='name')
# # print(supplier_trading_merged)
# supplier_trading_info, failures = put_supplier_trading(supplier_trading_merged)
# print(failures)
# supplier_trading_info.to_excel(excel, "supplier_trading")
# excel.save()

# # Put supplier payment info
# supplier_contact_info = pd.DataFrame(pd.read_excel(
#     'supplier_output_temp.xlsx',
#     "supplier_contact"))
# supplier_contact_info = supplier_contact_info[['supplier_name', 'supplier_id']]
# supplier_payment_merged = pd.merge(
#     supplier_contact_info, c, left_on='supplier_name', right_on='name')
# # print(supplier_payment_merged)
# supplier_payment_info, failures = put_supplier_payment(supplier_payment_merged)
# supplier_payment_info.to_excel(excel, 'supplier_payment')
# excel.save()
# print(failures)
