import json
import pandas as pd
import api_requests

BRANDS        = ['name', 'code', 'manufacturer_id', 'brand_type']
MANUFACTURERS = ['name', 'code']
BRANDS_TYPE   = {'Private': 0, 'National': 1, 'Multinational': 2}


def read_brands_manufacturers(excel_file):
    brands_manufacturers = pd.DataFrame(pd.read_excel(excel_file))
    brands_df            = pd.DataFrame(columns = BRANDS)
    manufacturers_df     = pd.DataFrame(columns = MANUFACTURERS)

    # Mapping brands
    brands_df['name']            = brands_manufacturers['Brand Name'].str.strip()
    brands_df['code']            = brands_manufacturers['Brand Code']
    brands_df['manufacturer_id'] = brands_manufacturers['Manufacturer Name']
    brands_df['brand_type']      = brands_manufacturers['Brand Type']
    brands_df                    = brands_df.drop_duplicates('name')

    # Mapping manufacturers
    manufacturers_df['name'] = brands_manufacturers['Manufacturer Name'].str.strip()
    manufacturers_df['code'] = brands_manufacturers['Manufacturer Code']
    manufacturers_df         = manufacturers_df.drop_duplicates('name')
    
    # Replace NaN with empty
    brands_df.fillna(value="", inplace=True)
    manufacturers_df.fillna(value="", inplace=True)

    # Replace with valid value
    brands_df = brands_df.replace({'brand_type': BRANDS_TYPE})
    return manufacturers_df, brands_df


def post_manufacturers(manufacturers_df: pd.DataFrame):
    response_success = list()
    response_failed  = list()
    manufacturers_df.fillna(value="", inplace=True)
    response_success_df = pd.DataFrame()
    for index, row in manufacturers_df.iterrows():
        manufacturers_info = json.loads(row.to_json())
        response = api_requests.post_product_manufacturers(manufacturers_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(manufacturers_info)
    response_success_df = pd.DataFrame(pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


def post_brands(brands_df: pd.DataFrame):
    response_success = list()
    response_failed  = list()
    brands_df.fillna(value="", inplace=True)
    
    # Get manufacturers and replace with id
    manufacturers = api_requests.get_product_manufacturers()
    manufacturers = pd.DataFrame(manufacturers)
    manufacturers = manufacturers[['name', 'id']].set_index('name')
    manufacturers = manufacturers.to_dict()['id']
    brands_df = brands_df.replace({'manufacturer_id': manufacturers})

    response_success_df = pd.DataFrame()
    for index, row in brands_df.iterrows():
        brands_info = json.loads(row.to_json())
        response = api_requests.post_product_brands(brands_info)
        if response.status_code in [200, 201]:
            response_success.append(response.json())
        else:
            response_failed.append(response.text)
            response_failed.append(brands_info)
    response_success_df = pd.DataFrame(pd.read_json(json.dumps(response_success)))
    return response_success_df, response_failed


manuf, bra = read_brands_manufacturers('brands_manufacturers.xlsx')
# print(bra.count())
res_manuf, failures = post_manufacturers(manuf)
print(failures)

res_bra, failures = post_brands(bra)
print(failures)
